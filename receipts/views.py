from django.shortcuts import redirect, render
from django.views.generic import ListView

from .form import AccountForm, ExpenseCategoryForm, ReceiptForm
from .models import Account, ExpenseCategory, Receipt
from django.contrib.auth.decorators import login_required
from django.db.models import Count
from django.urls import reverse 

# Create your views here.


@login_required
def receipts_list(request):
    receipts_lists = Receipt.objects.filter(purchaser=request.user)
    context = {
        'receipts_lists': receipts_lists
    }

    return render(request, 'receipts/receipts_list.html', context)



@login_required
def create_receipt_view(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm()

    return render(request, 'receipts/create_receipt.html', {'form': form})



@login_required
def category_list_view(request):
    categories = ExpenseCategory.objects.filter(
        owner=request.user
    ).annotate(
        receipt_count=Count('receipts__id')
    ).distinct()
    return render(request, 'receipts/category_list.html', {'categories': categories})



@login_required
def account_list_view(request):
    accounts = Account.objects.filter(receipts__purchaser=request.user).annotate(
        receipt_count=Count('receipts__id')
    )
    return render(request, 'receipts/account_list.html', {'accounts': accounts})



@login_required
def create_category_view(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = ExpenseCategoryForm()

    return render(request, 'receipts/create_category.html', {'form': form})



@login_required
def create_account_view(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = AccountForm()

    return render(request, 'receipts/create_account.html', {'form': form})
