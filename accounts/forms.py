from django import forms
from django.contrib.auth.models import User

class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)




class SignupForm(forms.Form):
    username = forms.CharField(max_length=150, label="username")
    password = forms.CharField(max_length=150, widget=forms.PasswordInput, label="password")
    password_confirmation = forms.CharField(max_length=150, widget=forms.PasswordInput, label="password_confirmation")


    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        password_confirmation = cleaned_data.get("password_confirmation")
        if password and password_confirmation and password != password_confirmation:
            self.add_error('password_confirmation', "Passwords do not match.")
        return cleaned_data
